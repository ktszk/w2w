#!/usr/bin/env python
# -*- coding:utf-8
#$ -cwd
#$ -V -S /usr/local/Python-2.7.12/bin/python
#$ -N CaFeAsF
#$ -e out.e
#$ -o out.o
#$ -pe smp 40
##$ -pe fillup 64
#$ -q salvia.q
##$ -q lantana.q
##for LSF (lava) options
#BSUB -q "queue_name"
#BSUB -n thread_num
#BSUB -J "Job_name"
#BSUB -o std_output_file
#BSUB -m "node_name"

#w2w.py wien2wannier python script
#if use wannier don't use oc or ocall
if __name__ == "__main__": #=======parameter================
    #material name and brave lattice name-------------------
    space='P4/mmm'; atom=['Ba','K','Fe','As','As']
    strc=[3.90901,3.90901,13.21224,90.0,90.0,90.0]
    astr=[[0.,0.,0.],[0.5,0.5,0.5],[0.5,0.,0.25],[0.,0.5,0.25],[0.,0.,0.35381],[0.5,0.5,0.14619]]
    #=========================switch========================
    init_VCA=0
    if init_VCA==1: #change .struct, .in2, .inst after initialize
        wien=1; rlapw=0; dos=0; gspa=0; spa=0
        wan_klist=0; w2w=0; wan=0; gnu=0
    else:
        wien=0; rlapw=0; dos=0; gspa=0; spa= 0 #Wien switch
        wan_klist=0; w2w=1; wan=1; gnu=0       #wannier switch
    t1=7                                       #choose band plot set
    #sometimes use
    eden=0; xspec=0 #NO Implementation #electr-density, x-ray spectra
    wplot=0         #NO Implementation #get wannier orbital
    oc=0; ocall=1                   #if wanna see orbital character oc=1
    cif=0                           #switch import cif file or not
    cifname='BaKFe4As4'             #cif file name
    sw_str=0; shift=1               #make struct , use shift Energy
    clean=0                         #if remove *broyd* and xsf.gz clean=1
    n_num=2
    #======================Wien2k parameter================
    default=0                       #if use default parameter default=1
    batch=1                         #if use interactive mode batch=0
    #-------------------------init parameters--------------
    num_wien=10; vxc=19; rmt_red=0; ecut=-6.0; rkmax=7.0; mix=0.1; fmt=0; sp=0
    # vxc LDA 12, GGA-PBE 13 GGA-PBEsol 19
    #-------------------run_lapw parameters----------------
    ec=0.0001; cc=0.0001; loop_num=40; fc=0; so=0; mpi=0; in1new=1; ni=0
    run_sw=0
    #======================spaghetti parameter=============
    k_num=200                       #band k_list num
    e_min=-3.5; e_max=3.0            #boiling spagehetti range
    jatom=0; jtype=1; soh=0.4       #jtype: tot=1,s=2,p=3,d=6
    #======================wannier parameter===============
    #-----------------energy range and min and max band----
    win_min=-10.; win_max=10.      #window range
    f_min=-0.0; f_max=0.0           #frozen range
    #--------projection and wannier centor atom's number---
    num=8                           #num kpoints
    watom=['Fe']                    #select atom number(1~) or name
    prj=['dFe1','dFe1',
         'dFe2','dFe2']             #wannier projection
    wcan=[]                         #use only if you wanna select atom directly
    fsp=1; bxsf_num=100             #bxsf switch bxsf grid point default 50
    #===================wplot parameter==================
    mode='ORTHO'                    #mode is 'ORTHO' or 'NO-ORTHO'
    orig=[0,0,0,1]                  #start wannier grid point
    xend=[2,0,0,1]                  #x end point 
    yend=[0,2,0,1]                  #y end point
    zend=[0,0,2,1]                  #z end point
    grid=[30,30,30]                 #pwscf's grid [60,60,96]
    ppo='NO'                        #ppo only 'DEP' or 'NO'
    pu='ANG'                        #pu only ANG or AU or ATU
    we='LARGE'                      #we only LARGE or SMALL
    iskpt=1; iswann=1;  wiw='WAN'   #
    #=================optional parameters===============
    #-----------------spin-orbit parameter--------------
    lmax=3; n_RLO=[]; nSO=[]
    e_lo=[]                         #def e-lo=-4.97
    de=[]                           #de=0.0005
    desw=[]
    so_emin=-10.0; so_emax=1.5
#=================Physical constants==================
Ry=round(13.60568,1); bohr=round(0.52917721067,6)
#=================import_class=========================
import os,shutil,sys
#=================defined functions=================
for f in open('w2w.py','r'):
    #if f.find('-n')!=-1 and f.find('BSUB')!=-1:
    if f.find('smp')!=-1 and f.find('-pe')!=-1:
        ompt=int(f.split()[3])
        break
def check_module(module):
    import pkgutil
    c=[b[1] for b in pkgutil.iter_modules()]
    bo=module in c
    return bo

def mkinso(name,lmax,emin,emax,nSO,n_RLO,e_lo,de,desw,atom):
    hkl=[0,0,1]
    t1='llmax,ipr,kpot'
    t2='Emin,Emax'
    t3='h,k,l (direction of magnetization)'
    t4='number of atoms with RLO'
    t5='atom number,e-lo,de (case.in1), repeat NX times'
    t6='number of atoms without SO, atomnumbers'
    if len(n_RLO)!=0:
        if len(de)!=len(n_RLO):
            de=[0.0005 for i in n_RLO]
        if len(desw)!=len(n_RLO):
            desw=['C' for i in n_RLO]
        if len(e_lo)!=len(n_RLO):
            e_lo=[-0.497 for i in n_RLO]
        for i in xrange(len(desw)):
            desw[i]=desw[i].replace('S','STOP').replace('C','CONT')
            desw[i]=desw[i].replace('STOPTOP','STOP').replace('CONTONT','CONT')
            if(desw[i]!='STOP' and desw[i]!='CONT'):
                print("please input 'S' or 'C' or 'STOP' or 'CONT'\n")
                exit()
    f=open('%s.inso'%name,'w')
    f.write('WFFIL\n%d  0  0  %30s\n%6.2f %6.2f  %19s\n'
            %(lmax,t1,emin,emax,t2))
    f.write('    %d %d %d  %48s\n %d  %45s\n'%tuple(hkl+[t3,len(n_RLO),t4]))
    for i,nR in enumerate(n_RLO):
       f.write('%d %7.3f %7.4f %s %49s\n'%(nR,e_lo[i],de[i],desw[i],t5))
    f.write('%d'%len(nSO))
    if len(nSO)==0:
        f.write('  %d  %58s\n'%(len(atom),t6))
    else:
        for nS in nSO:
            f.write(' %d'%nS)
        f.write('%s\n'%t5)
    f.close()

def modin1(name):
    fp=open('tmp','w')
    for f in open('%s.in1'%name,'r'):
        fp.write(f.replace('2.5','5.0') if 'red' in f else f)
    fp.close()
    shutil.move('tmp','%s.in1'%name)

def get_z(atom):
    atoms=('H' ,                                                                                'He',
           'Li','Be',                                                  'B' ,'C' ,'N' ,'O' ,'F' ,'Ne',
           'Na','Mg',                                                  'Al','Si','P' ,'S' ,'Cl','Ar',
           'K' ,'Ca','Sc','Ti', 'V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr',
           'Rb','Sr','Y' ,'Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I' ,'Xe',
           'Cs','Ba',
                     'La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu',
                          'Hf','Ta','W' ,'Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn',
           'Fr','Ra',
                     'Ac','Th','Pa','U' ,'Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr',
                          'Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg','Cn','Uut')
    if(atom in atoms):
        return(atoms.index(atom)+1)
    else:
        print("Atom name isn't found")
        exit()

def cartesian(kvec,brav): #-----------------base change cartesian-------------
    if brav=='P':
        carte=[[1.,0.,0.],
               [0.,1.,0.],
               [0.,0.,1.]]
    elif brav=='I':
        carte=[[0.,1.,1.],
               [1.,0.,1.],
               [1.,1.,0.]]
    elif brav=='F':
        carte=[[-1., 1., 1.],
               [ 1.,-1., 1.],
               [ 1., 1.,-1.]]
    elif brav=='C':
        carte=[[1.,-1.,0.],
               [1., 1.,0.],
               [0., 0.,1.]]
    temp=[sum(ct*kv for ct,kv in zip(cr,kvec)) for cr in carte]
    return(temp)

def point_len(strc,p_num,km,brav): #--------fix k_point length----------
    from math import sqrt
    for i,pn in enumerate(p_num):
        p_num[i]=cartesian(pn,brav)
    len_kax=[1./st for st in strc[:3]]
    len_k=[sqrt(sum((pn2[j]-pn[j])*(pn2[j]-pn[j])*lk*lk
                    for j,lk in enumerate(len_kax)))
           for pn,pn2 in zip(p_num,p_num[1:])]
    len_all=sum(len_k)
    con=[int(round(km*lk/len_all)) for lk in len_k]
    con2=[km*lk/len_all for lk in len_k]
    N=[cn*2 for cn in con]
    N.append(N[-1])
    return(N,con)

def klist_band(brav,t1): #----------make klist_offset-------------
    if brav=='P':
        if t1==1:
            p_nam=['R','GAMMA','X','M','GAMMA']
            p_num=[[0.5,0.5,0.5],[0,0,0],[0.5,0,0],[0.5,0.5,0],[0,0,0]]
        elif t1==2:
            p_nam=['GAMMA','X','M','GAMMA','Z','R','A','Z']
            p_num=[[0,0,0],[0.5,0,0],[0.5,0.5,0],[0,0,0],[0,0,0.5],
                   [0.5,0,0.5],[0.5,0.5,0.5],[0,0,0.5]]
        elif t1==3:
            p_nam=['A','Z','GAMMA','M']
            p_num=[[0.5,0.5,0.5],[0,0,0.5],[0,0,0],[0.5,0.5,0]]
        elif t1==4:
            p_nam=['GAMMA','X','M','GAMMA']
            p_num=[[0,0,0],[0.5,0,0],[0.5,0.5,0],[0,0,0]]
        elif t1==5:
            p_nam=['Z','GAMMA','X','M','GAMMA','Z','R','A','Z']
            p_num=[[0,0,-0.5],[0,0,0],[0.5,0,0],[0.5,0.5,0],
                   [0,0,0],[0,0,0.5],[0.5,0,0.5],
                   [0.5,0.5,0.5],[0,0,0.5]]
        elif t1==6:
            p_nam=['GAMMA','M','K','GAMMA','A']
            p_num=[[0,0,0],[0.5,0.0,0],[1.0/3,1.0/3,0],
                   [0,0,0],[0,0,0.5]]
        elif t1==7:
            p_nam=['GAMMA','Y','T','R','S','X','U','Z','GAMMA']
            p_num=[[0.,0.,0.],[0.,.5,0.],[0.,.5,.5],[.5,.5,.5],
                   [.5,.5,0.],[.5,0.,0.],[0.5,0.,0.5],
                   [0.,0.,0.5],[0.,0.,0.]]
    elif brav=='I':
        if t1==1:
            p_nam=['Z','GAMMA','X','P','N','GAMMA']
            p_num=[[0.5,0.5,-0.5],[0.0,0.0,0.0],[0.0,0.0,0.5]
                    ,[0.25,0.25,0.25],[0.0,0.5,0.0],[0.0,0.0,0.0]]
        elif t1==2: #Graser use
            p_nam=['GAMMA','X','M','GAMMA','Z','R','A','M']
            p_num=[[0.0,0.0,0.0],[-0.25,0.25,0.25],[-0.5,0.5,0.0]
                    ,[0.0,0.0,0.0],[0.5,0.5,-0.5],[0.25,0.75,-0.25]
                    ,[0.0,1.0,-0.5],[-0.5,0.5,0.0]]
        elif t1==3:
            p_nam=['GAMMA','X','Z','GAMMA','Z']
            p_num=[[0.0,0.0,0.0],[-0.5,0.5,0.0],[-0.5,0.5,0.5]
                    ,[0.0,0.0,0.0],[0.5,0.5,-0.5]]
        elif t1==4:
            p_nam=['A','Z','GAMMA','M']
            p_num=[[0.0,1.0,-0.5],[0.5,0.5,-0.5],
                   [0.0,0.0,0.0],[-0.5,0.5,0.0]]
    elif brav=='F':
        p_nam=['Z','GAMMA','X','M','GAMMA']
        p_num=[[0.5,0.5,-0.5],[0,0,0],[-0.25,0.25,0.25],[-0.5,0.5,0],[0,0,0]]
    else:
        p_nam=['GAMMA','X','M','Y','GAMMA','M']
        p_num=[[0,0,0],[0.5,0,0],[0.5,0.5,0],[0,0.5,0],[0,0,0],[0.5,0.5,0]]
    return(p_nam,p_num)

def mkklist(name,brav,p_nam,p_num,N,con): #---------make klist_band-----------
    f=open('%s.klist_band' %(name),'w')
    p_num1=[cartesian(pnu,brav) for pnu in p_num]
    for pna,pnu,pnu1,nn,co in zip(p_nam,p_num1,p_num1[1:],N,con):
        f.write('%-10s %4d %4d %4d %4d  2.0'
                %(pna,int(pnu[0]*nn),int(pnu[1]*nn),int(pnu[2]*nn),nn))
        f.write('-8.00 8.00\n' if pna==p_nam[0] else '\n')
        for j in xrange(1,co):
            f.write('%-10s %4d %4d %4d %4d  2.0\n'
                    %('',int((pnu1[0]-pnu[0])*nn/co*j+pnu[0]*nn)
                      ,int((pnu1[1]-pnu[1])*nn/co*j+pnu[1]*nn)
                      ,int((pnu1[2]-pnu[2])*nn/co*j+pnu[2]*nn),nn))
    f.write('%-10s %4d %4d %4d %4d  2.0\nEND\n'
            %(p_nam[-1],int(p_num1[-1][0]*N[-1])
              ,int(p_num1[-1][1]*N[-1]),int(p_num1[-1][2]*N[-1]),N[-1]))
    f.close()

def klist_wan(name,num,brav):  #--------make klist for wannier-------------
    f=open('%s.klist' %(name),'w')
    count=0
    for i in xrange(num):
        for j in xrange(num):
            for k in xrange(num):
                vec=cartesian([i,j,k],brav)
                count=count+1
                f.write('%10d%10d%10d%10d%10d%5.1f'
                        %tuple([count]+vec+[num,1.0]))
                f.write('%5.1f%5.1f%10d k, div: (%3d%3d%3d)\n'
                        %tuple([-7.0,1.5,0]+[num]*3) 
                        if (i==0 and j==0 and k==0) else '\n')
    f.write('END\n')
    f.close()

def get_eps(name,outname,emin,emax):
    def get_spamax(name):
        maxlen=0.
        for f in open('%s.spaghetti_ene'%name,'r'):
            item=f.split()
            if f.find('bandindex')==-1:
                maxlen=float(item[3]) if float(item[3]) > maxlen else maxlen
            else:
                if int(item[1])>1:
                    break
        return(maxlen)
    if check_module('matplotlib'):
        import numpy as np
        import matplotlib.pyplot as plt
    else:
        a=sys.version_info
        print('Python ver. %d.%d.%d'%(a[0],a[1],a[2]))
        if a[0]>=2 and a[1]>=4 and a[2]>=4:
            from subprocess import Popen, PIPE
            gp=Popen('gnuplot',stdin=PIPE)
            gp.stdin.write('set xrange[0:%f]\n'%get_spamax(name))
            gp.stdin.write('set yrange[%f:%f]\n'%(emin,emax))
            gp.stdin.write('set term postscript enhanced eps color\n')
            gp.stdin.write('set output "%s.eps"\n'%outname)
            gp.stdin.write('plot "%s_band.dat" u($1*%8.6f):2 w l,'%(name,bohr))
            gp.stdin.write('"%s.spaghetti_ene" u 4:5, 0\nquit\n'%name)
            gp.kill()
        else:
            gp=os.popen('gnuplot','w')
            print >>gp,'set xrange[0:%f]'%get_spamax(name)
            print >>gp,'set yrange[%f:%f]'%(emin,emax)
            print >>gp,'set term postscript enhanced eps color'
            print >>gp,'set output "%s.eps"'%outname
            print >>gp,('''plot "%s_band.dat" u($1*%8.6f):2 w l,'''
                        '''"%s.spaghetti_ene" u 4:5, 0'''%(name,bohr,name))
            print >>gp,'quit'
            gp.flush()
            gp.close()
    print('output %s.eps'%outname)

def os_and_print(com):
    import os
    print(com); os.system(com)

def date():
    from datetime import datetime
    from locale import setlocale, LC_ALL
    d=datetime.today()
    setlocale(LC_ALL,'')
    if os.environ['LANG'].find('ja')!=-1:
        print(d.strftime('%Y年 %B %d日 %A %H:%M:%S JST'))
    else:
        print(d.strftime('%a %b %d %X JST %Y'))

#---------w2w spin-orbit----------
def mk_wfdef(name):
    for sp in ['up','dn']:
         iolist=[[5,'w2win%s'%sp,'old','formatted',0],
                 [6,'outputwf_%s'%sp,'unknown','formatted',0],
                 [7,'amn_%s'%sp,'unknown','formatted',0],
                 [8,'mmn_%s'%sp,'unknown','formatted',0],
                 [9,'vectorso%s'%sp,'unknown','unformatted',9000],
                 [10,'nnkp','old','formatted',0],
                 [12,'eig','unknown','formatted',0],
                 [18,'vsp%s'%sp,'old','formatted',0],
                 [20,'struct','old','formatted',0],
                 [21,'scfwf_%s'%sp,'unknown','formatted',0],
                 [50,'energyso%s'%sp,'unknown','formatted',9000]]
         f=open('wf%s.def'%sp,'w')
         for lst in iolist:
             f.write("%2d, %s.%s, '%s', '%s', %d\n"
                     %tuple(lst[:1]+[name]+lst[1:]))
         f.close()
#----------LDA+U------------------
def mkinorb(self,name,nmod,atom):
    f=open('%s.inorb'%name,'r')
    f.write('%d %d %d'%(nmod,len(atom),ipr)
            +'%21snmod, natorb, ipr\n'%'')
    f.write('PRATT, 1.0%21sBROYD/PRATT, mixing\n'%'')
    for i in atom:
        for j in ipr:
            f.write('%d %d %d%24siatom nlorb, lorb\n'%(1,1,1,''))
    if nmod==1: #LDA+U
        f.write(' %d%30snsic 0..AFM, 1..SIC, 2..HFM\n'%(nsic,''))
        for i,at in enumerate(atom):
            for j,ip in enumerate(ipr):
                f.write('%4.2f %4.2f%8sU J (Ry)  \n'%(U[i,j],J[i,j],''))
    elif nmod==2: #Orbital Polarization
        f.write('')
        for i in natorb:
            f.write('')
    elif nmod==3: #B_ext
        f.write('%d\n%d %d %d'%tuple([1]*4))
    f.colse
def mkindm(name,atom):
    f=open('%s.inorb'%name,'r')
    f.write('%d.%22sEmin cutoff energy\n')
    f.write(' %d%23snumber of atoms for which density matrix is calculated\n'
            %(len(atom),''))
    for i in atom:
        f.write(" %d  %d      index of 1st atom, number of L's, L1\n"
                %(1,4))
        f.write('0           r-index, (l,s)index\n')
    f.colse
#===================defined classes==============================-----
class Material(object):
    __slots__=['name','space','atom','strc','astr','cifname']
    def __init__(self,name,space,atom,strc,astr,cifname):
        self.name=name
        self.space=space
        self.atom=atom
        self.strc=strc
        self.astr=astr
        self.cifname=cifname
    def cif2strut(self):
        if self.cifname=='':
            self.cifname=self.name
        os_and_print('cif2struct %s.cif'%cifname)
    def matfromstruct(self):
        f=open('%s.struct'%self.name,'r')
        try:
            self.name=f.readline().strip()
            tmp=f.readline().split(':')
            self.space=tmp[1][3:]
            f.readline()
            tp=f.readline()
            tmp=tp.split()
            if len(tmp)!=6:
                tp.replace('001','00 1')
                tmp=tp.split()
            self.strc=[float(tp) for tp in tmp]
            self.strc[:3]=[st*bohr for st in strc[:3]]
            self.atom=[]
            tmp=f.leadline()
            while tmp:
                if 'RMT' in tmp:
                    self.atom.append(tmp[0:2].strip())
                tmp=f.readline()
        finally:
            f.close()
    def atom_num(self):
        atom_num=[]; i=0
        for f in open('%s.struct'%self.name,'r'):
            if 'X=' in f:
                atom_num.append([self.atom[i],i+1])
            if 'RMT' in f:
                i=i+1
        return(atom_num)
    def brav(self,sw):
        if 'P' in self.space:
            brav='P'
        elif 'I' in self.space:
            if sw==0: brav='I'
            if sw==1: brav='B'
        elif 'F' in self.space:
            brav='F'
        elif 'A' in self.space:
            brav='A'
        elif 'C' in self.space:
            brav='C'
        elif 'R' in self.space:
            brav='R'
        return(brav)
    def getef(self,ext):
        for f in open('%s.%s'%(self.name,ext),'r'):
            if 'FER'in f:
                item=f.split('=')
        return float(item[1])
    def get_band_num(self): #---get max and min bands------
        tmax=0; tmin=int(1E5)
        for f in open('%s.find_bands' %self.name,'r'):
            if 'k=' in f:
                item=f.replace('|','=').split('=')
                tmin=int(item[5]) if int(item[5])<tmin else tmin
                tmax=int(item[7]) if int(item[7])>tmax else tmax
        return [tmin,tmax]
    def struct(self): #make .struct file--
        def get_z_r0(atom):
            z=get_z(atom)
            print z
            r0=(1E-5 if 36<z<=71 else 5E-5 if 
                18<z<=36 else 1E-4 if z<=18 else 5E-6)
            return(r0,z) #End get_z
        if('2' in self.space or '4' in self.space):
            #struct main
            tstmp=lambda a:[1.0-ast if ast!=0. else 0.0 for ast in astr[i]]
            tst=[st/bohr for st in self.strc[:3]]
            ts=[[] if tstmp(astr[i])==self.astr[i] else [tstmp(astr[i])]
                for i,at in enumerate(self.atom)]
            f=open('%s.struct' %(self.name),'w')
            f.write('%s\n%s   LATTICE,NONEQUIV.ATOMS: %2d%s\n'
                    %(self.name,self.brav(1),len(self.atom),self.space))
            f.write('MODE OF CALC=RELA unit=%s\n'%'ang')
            f.write(' %9.6f %9.6f %9.6f %9.6f %9.6f %9.6f\n'
                    %tuple(tst+self.strc[3:]))
            for i,at in enumerate(self.atom):
                (r0,z)=get_z_r0(at)
                an=-(i+1)
                f.write('ATOM  %2d: X=%10.8f Y=%10.8f Z=%10.8f\n'%tuple([an]+self.astr[i]))
                f.write('%10sMULT= %d%10sISPLIT= %d\n' %('',len(ts[i])+1,'',8))
                for its in ts[i]:
                    f.write('ATOM  %2d:X= %10.8f Y=%10.8f Z=%10.8f\n'%tuple([an]+its))
                f.write('%-2s%9sNPT= %4d  R0=%10.8f '%(at,'',781,r0))
                f.write('RMT=%10.4f   Z:%5.1f\n'%(2.0,z))
                lrm=['LOCAL ROT MATRIX:','','']
                temp='%-21s%9.7f %9.7f %9.7f\n'
                if i==0:
                    f.write(temp%(lrm[0],1.0,0.0,0.0))
                    f.write(temp%('',0.0,1.0,0.0))
                    f.write(temp%('',0.0,0.0,1.0))
                else:
                    for j in xrange(3):
                        f.write(temp%(lrm[j],0.0,0.0,0.0))
            f.write('   0      NUMBER OF SYMMETRY OPERATIONS')
            f.close()
    def get_olist(self):
        f=open('%s.qtl'%self.name,'r')
        f.readline(); f.readline(); f.readline(); f.readline()
        temp=[f.readline().split() for at in self.atom]
        olist=[tmp[-1].split(',') for tmp in temp]
        f.close()
        return(olist)
    def tetra_int(self,swt):
        dlist=('tot','0','1','2','3')
        olist=self.get_olist()
        if swt:
            ndos=5*len(olist)
        else:
            ndos=0
            for ol in olist:
                ndos=ndos+len(ol)
        f=open('%s.int'%self.name,'w')
        f.write('Title\n %4.2f %5.3f %5.3f %5.3f  '%(-0.5,0.002,1.5,0.003))
        f.write('# EMIN, DE, EMAX, Gauss-broadening(>;de))\n')
        f.write('%5s    N   0.000        # NUMBER OF DOS-CASES specified'%ndos)
        f.write(' below, G/L/B broadening (Ry)\n    0    1   total        ')
        f.write('# atom, case=column in qtl-header, label\n')
        for i,olis in enumerate(olist):
            for j,ol in enumerate(olis):
                if(swt and not ol in dlist):
                    pass
                else:
                    f.write('%5d%5d   Atom%d %s\n'%(i+1,j+1,i+1,ol))
        f.close()
class Opt(object):
    __slot__=['default','batch','num_k','vxc','rmt_red','ecut','rkmax','mix',
              'fmt','sp','ec','cc','fc','loop_num','so','mpi','in1new','ni',
              'run_sw']
    def __init__(self):
        self.default=1
    def setup(self,default,batch,num_k,vxc,rmt_red,ecut,rkmax,mix,fmt,sp,
                 ec,cc,fc,loop_num,so,mpi,in1new,ni,run_sw):
        self.default=default; self.batch=batch; self.num_k=num_k
        self.vxc=vxc; self.rmt_red=rmt_red; self.ecut=ecut
        self.rkmax=rkmax; self.mix=mix; self.fmt=fmt; self.sp=sp
        self.ec=ec; self.cc=cc; self.fc=fc; self.loop_num=loop_num
        self.so=so; self.mpi=mpi; self.in1new=in1new; self.ni=ni
        self.run_sw=run_sw
    def options(self):
        init=''; run=''
        if self.default==0:
            if self.batch==0:
                print('start setting')
            else:
                print('Batch mode')
                init=init+'-b'
                if self.rmt_red <6:
                    init=init+' -red %d'%self.rmt_red
                else:
                    print('rmt_red use only 0 ~ 5')
                if self.vxc!=0:       init=init+' -vxc %d'%self.vxc
                if self.ecut!=0:      init=init+' -ecut %4.1f'%self.ecut
                if self.rkmax!=0:     init=init+' -rkmax %4.1f'%self.rkmax
                if self.fmt!=0:       init=init+' -fermit %f'%self.fmt
                if self.mix!=0:       init=init+' -mix %3.1f'%self.mix
                if self.sp!=0:        init=init+'-sp'
                init=init+' -numk %d'%self.num_k
                if self.in1new!=0:    run=run+'-in1new %d '%self.in1new
                if self.loop_num!=0:  run=run+' -i %d'%self.loop_num
                if self.ec!=0:        run=run+' -ec %f'%self.ec
                if self.cc!=0:        run=run+' -cc %f'%self.cc
                if self.fc!=0:        run=run+' -fc %f'%self.fc
                if self.mpi!=0:       run=run+' -p'
                if self.so==1:        run=run+' -so'
                if self.ni==1:        run=run+' -NI'
        return(init,run)
    def lstart(self):
        p=os.popen('x lstart','w')
        print >>p,'%d'%self.vxc
        print >>p,'%d'%self.ecut
        p.flush()
        p.close()
    def xkgen(self):
        p=os.popen('x kgen','w')
        print >>p,'%d'%self.num_k
        print >>p,'0'
        p.flush()
        p.close()
    def init(self,n_num):
        def nn(n_num):
            p=os.popen('x nn','w')
            print >>p,'%d'%n_num
            p.flush()
            p.close()
        nn(n_num)
        os.system('x sgroup')
        os.system('x symmetry')
        self.lstart()
        self.xkgen()
        os.system('x dstart')
class Switch(object):  #switch class have switching for main function 
    __slot__=['wien','rlapw','dos','eden','xspec','gspa','spa',
              'wan_klist','w2w','wan','wplot','gnu',
              'oc','ocall','sw_str','shift','clean','t1','cif']
    def __init__(self):
        self.wien=1; self.rlapw=1; self.dos=1; self.eden=0; self.xspec=0
        self.gspa=1; self.spa=1; self.wan_klist=1; self.w2w=1; self.wan=1
        self.wplot=0; self.gnu=1; self.oc=0; self.ocall=0; self.sw_str=1
        self.shift=1; self.clean=1; self.t1=1; self.cif=1
    def setup(self,wien,rlapw,dos,eden,xspec,gspa,spa,wan_klist,
              w2w,wan,wplot,gnu,oc,ocall,sw_str,shift,clean,t1,cif):
        self.wien=wien; self.rlapw=rlapw
        self.dos=dos; self.eden=eden; self.xspec=xspec
        self.gspa=gspa; self.spa=spa
        self.wan_klist=wan_klist; self.w2w=w2w; self.wan=wan
        self.wplot=wplot; self.gnu=gnu
        self.oc=oc; self.ocall=ocall
        self.sw_str=sw_str; self.shift=shift; self.clean=clean; self.t1=t1
        self.cif=cif
class Optspa(object):
    """Optspa class have spaghetti options and function to make .insp file"""
    __slot__=['k_num','min','max','atom','type','soh']
    def __init__(self,knum,emin,emax,atom,atype,soh):
        self.k_num=knum; self.e_length=[emin,emax]
        self.param=[atom,atype,soh]
    def insp(self,ef,name): #-----------make insp file-----------------
        f=open('%s.insp'%name,'w')
        temp=('''### Figure configuration\n'''
              ''' 5.0   3.0%25s# paper offset of plot\n'''
              '''10.0  15.0%25s# xsize,ysize [cm]\n'''
              ''' 1.0   4%27s# major ticks, minor ticks\n'''
              ''' 1.0   1%27s# character height, font switch\n'''
              '''1.1   2    4%22s# line width, line switch, color switch\n'''
              '''### Data configuration\n'''
              ''' %8.4f%8.4f 2%16s# energy range, '''
              '''energy switch (1:Ry, 2:eV)\n'''
              '''1      %7.5f%21s# Fermi switch,  '''
              '''Fermi-level (in Ry units)\n'''
              '''1   999%28s# number of bands for heavier plotting   1,1\n'''
              '''%d      %d    %3.1f%20s# jatom, jtype, size  of heavier '''
              '''plotting\n\n\nFermi switch:\n  0...no line\n'''
              '''  1...solid line\n'''
              '''  2...dashed line\n  3...dotted line\n\nLine switch:\n'''
              '''  0...dots\n  1...lines\n  2...lines and open circle\n'''
              '''  3...lines and filled circles\n\n'''
              '''Color switch (re-define your colors in defins.f)\n'''
              '''  0...black\n  1...one-color plot\n  2...three-color plot\n'''
              '''  3...multi-color plot\n  4...multi-color plot,'''
              ''' one color for each irr. representations\n\nFont switch:\n'''
              '''  0...no text\n  1...Times and Symbol\n'''
              '''  2...Times, Symbol, and Times-Italic\n'''
              '''  3...Helvetica, Symbol, and  Helvetica-Italic\n'''
              '''  4...(include your own fonts in defins.f)''')
        f.write(temp %tuple(['']*5+self.e_length+['',ef]+['']*2+self.param+['']))
        f.close()
class Optwan(object):
    """Optwan class have wannier options and
    generate .win and .w2win file"""
    __slot__=['wmin','wmax','fmin','fmax','num','watom','wcan',
              'prj','fsp','bxnum']
    def __init__(self,wmin,wmax,fmin,fmax,num,watom,prj,fsp,bxnum):
        self.win_param=[wmax,wmin,fmax,fmin]
        self.num=num; self.watom=watom; self.wcan=[]
        self.prj=prj; self.fsp=fsp; self.bxsf_num=bxnum
    def bnum(self,sw):
        bnum=0
        for i,pr in enumerate(self.prj):
            if sw==1:
                if pr=='d':
                    self.prj[i]=['dz2','dxz','dyz','dxy','dx2-y2']
                if pr=='d2':
                    self.prj[i]=['dz2','dyz','dxz','dxy','dx2-y2']
                elif pr=='p':
                    self.prj[i]=['px','py','pz']
                elif pr=='deg':
                    self.prj[i]==['dz2','dx2-y2']
                elif pr=='dt2g':
                    self.prj[i]=['dxz','dyz','dxy']
                elif pr=='d4':
                    self.prj[i]=['dz2','dxz','dyz','dxy']
                elif pr=='dFe1':
                    self.prj[i]=['dz2','dXZ','dYZ','dxy','dx2-y2']
                elif pr=='dFe2':
                    self.prj[i]=['dz2','dYZ','dXZ','dxy','dx2-y2']
                elif pr=='f':
                    self.prj[i]=['fz(5z2-3r2)','fx(5z2-3r2)','fy(5z2-3r2)',
                                 'fz(x2-y2)','fxyz','fx(x2-3y2)','fy(3x2-y2)']
        bnum=sum(len(pr) for pr in prj)
        return(bnum)
    def win(self,name,p_nam,p_num,num_b,ef,so): #--------make .win flie--------
        log_fsp='true' if self.fsp else 'false'
        wan_num=2*self.bnum(1) if so else self.bnum(1)
        f=open('%s.win'%name,'w')
        f.write('num_bands%7s=  %d\nnum_wann%8s=  %d\n'
                %('',num_b[1]-num_b[0]+1,'',wan_num,))
        f.write('%snum_iter%8s= %d\n\n'%('' if so else '!','', 0 if so else 200))
        f.write('''dis_win_max     = %8.5f\ndis_win_min     = %8.5f\n'''
                '''dis_froz_max    =%9.6f\ndis_froz_min    =%9.6f\n'''
                %tuple(self.win_param))
        f.write('dis_num_iter    = 300\n!dis_mix_ratio   = 0.1\n\n')
        if so==1:
            f.write('spinors = .True.\n')
        f.write('''fermi_surface_plot = .%s.\n'''
                '''fermi_energy = %9.5f\nfermi_surface_num_points = %d\n'''
                '''bands_plot = .%s.\n\nBegin Kpoint_Path\n'''
                %(log_fsp,ef,self.bxsf_num,'true'))
        for i,pna in enumerate(p_nam):
            if pna=='GAMMA':
                p_nam[i]='G'
        for pna,pna1,pnu,pnu1 in zip(p_nam,p_nam[1:],p_num,p_num[1:]):
            f.write('%1s '%pna+'%5.2f %5.2f %5.2f '%tuple(pnu)
                    +'%1s '%pna1+'%5.2f %5.2f %5.2f\n'%tuple(pnu1))
        f.write('End Kpoint_Path\n\nbegin unit_cell_cart\nbohr\n')
        for i in open('%s.outputkgen' %name,'r'):
            if('R1' in i or 'R2' in i or 'R3' in i):
                item=i.replace('-',' -').split()
                f.write('%12.7f%12.7f%12.7f\n'
                        %(float(item[2]),float(item[3]),float(item[4])))
        f.write('end unit_cell_cart\n\n')
        f.write('begin atoms_frac\n')
        list=[]
        for lst in list:
            f.write('%2s %8.5f %8.5f %8.5f\n'%tuple(lst))
        f.write('end atoms_frac\n\n')
        f.write('begin projections\nend projections\n\n'
                +'mp_grid :%4d%4d%4d\n\nbegin kpoints\n'%tuple([self.num]*3))
        for i in xrange(self.num):
            for j in xrange(self.num):
                for k in xrange(self.num):
                    f.write('%16.12f%16.12f%16.12f\n'
                            %(1.0/self.num*i,1.0/self.num*j,1.0/self.num*k))
        f.write('end kpoints')
        f.close()
    def w2win(self,name,num_b,so): #---------make w2win file------------
        def projc(prj): #-------make prjection---------
            sq2=0.70710677; sq3=0.57735027
            if prj=='s':
                a=[[0,0,1.0,0]]
            elif prj=='px':
                a=[[1,-1,sq2,0.0],[1,1,sq2,0.0]]
            elif prj=='py':
                a=[[1,-1,sq2,0.0],[1,1,-sq2,0.0]]
            elif prj=='pz':
                a=[[1,0,1.0,0.0]]
            elif prj=='Px':
                a=[[1,-1,0.0,sq2],[1,1,0.0,sq2]]
            elif prj=='dxz' or prj=='dzx':
                a=[[2,-1,sq2,0.0],[2,1,sq2,0.0]]
            elif prj=='dyz' or prj=='dzy':
                a=[[2,-1,0.0,sq2],[2,1,0.0,-sq2]]
            elif prj=='dxy' or prj=='dyx':
                a=[[2,-2,sq2,0.0],[2,2,-sq2,0.0]]
            elif prj=='dx2-y2' or prj=='dx2y2':
                a=[[2,-2,sq2,0.0],[2,2,sq2,0.0]]
            elif prj=='dz2':
                a=[[2,0,1.0,0.0]]
            elif prj=='dXZ':
                a=[[2,-1,-0.5,0.5],[2,1,0.5,0.5]]
            elif prj=='dYZ':
                a=[[2,-1,0.5,0.5],[2,1,-0.5,0.5]]
            elif prj=='fz(5z2-3r2)':
                a=[[3,0,1.0,0.0]]
            elif prj=='fx(5z2-3r2)':
                a=[[3,-1,sq2,0.0],[3,1,sq2,0.0]]
            elif prj=='fy(5z2-3r2)':
                a=[[3,-1,sq2,0.0],[3,1,-sq2,0.0]]
            elif prj=='fz(x2-y2)':
                a=[[3,-2,sq2,0.0],[3,2,sq2,0.0]]
            elif prj=='fxyz':
                a=[[3,-2,sq2,0.0],[3,2,-sq2,0.0]]
            elif prj=='fx(x2-3y2)':
                a=[[3,-3,sq2,0.0],[3,3,sq2,0.0]]
            elif prj=='fy(3x2-y2)':
                a=[[3,-3,sq2,0.0],[3,3,-sq2,0.0]]
            elif prj=='sp2':
                a=[[0,0,sq3,0],[1,-1,sq3,0.0],[1,1,sq3,0]]
            elif prj=='sp3':
                a=[[0,0,0.5,0],[1,0,0.5,0],[1,-1,0.5,0.0],[1,1,0.5,0]]
            return(a) #End projc---------start w2win main program ---------
        if so==1:
            wan_num=2*self.bnum(0)
            spindep=['up','dn']
        else:
            wan_num=self.bnum(0)
            spindep=['']
        for sp in spindep:
            f=open('%s.w2win%s'%(name,sp),'w')
            f.write('''BOTH\n%4d%4d       # min band, max band\n'''
                    '''%4d%4d       # LJMAX max in exp(ibr) expansion,'''
                    '''#Wannier functions\n'''%(num_b[0],num_b[1],3,wan_num))
            for i,pr in enumerate(self.prj):
                if sp=='dn':
                    for jpr in pr:
                        f.write('0\n')
                for jpr in pr:
                    a=projc(jpr)
                    f.write('''%d%5s#number of entries per orbital '''
                            '''(list of projected orbit)\n''' %(len(a),''))
                    for aa in a:
                        f.write('''%3d%2d%3d%13.8f%13.8f%3s #index of atom,'''
                                ''' L, M, coefficient (complex)\n'''
                                %tuple([self.wcan[i]]+aa+['']))
                if sp=='up':
                    for jpr in pr:
                        f.write('0\n')
            f.close()
    def amn_total(self,name):
        tmp=[f.split() for f in open('%s.amn_up'%name,'r')]
        title=tmp[0][0]
        Nums=[int(tm) for tm in  tmp[1]]
        tmp0=[[int(i) for i in tp[:3]] for tp in tmp[2:]]
        tmp1=[[float(i) for i in tp[3:5]] for tp in  tmp[2:]]
        tmp=[f.split() for f in open('%s.amn_dn'%name,'r')]
        tmp2=[[float(i) for i in tp[3:5]] for tp in  tmp[2:]]
        tmp=[[t1[0]+t2[0],t1[1]+t2[1]] for t1,t2,in zip(tmp1,tmp2)]
        f=open('%s.amn'%name,'w')
        f.write('%s\n'%title)
        f.write('%9s%3d%9s%3d%9s%3d\n'
                %('',Nums[0],'',Nums[1],'',Nums[2]))
        for tp1,tp2 in zip(tmp0,tmp):
            f.write('%5d%5d%5d %21.12E%21.12E\n'%tuple(tp1+tp2))
        f.close()
    def mmn_total(self,name):
        f1=open('%s.mmn_up'%name,'r')
        f2=open('%s.mmn_dn'%name,'r')
        title=f1.readline().split()[0]
        Nums=[int(tm) for tm in f1.readline().split()]
        f2.readline()
        f2.readline()
        f3=open('%s.mmn'%name,'w')
        f3.write('%s\n'%title)
        f3.write('%9s%3d%9s%3d%9s%3d\n'
                %('',Nums[0],'',Nums[1],'',Nums[2]))
        for i in xrange(Nums[1]):
            for j in xrange(Nums[2]):
                tmp=[int(i) for i in f1.readline().split()]
                f2.readline()
                f3.write('%8d%8d%8d%8d%8d\n'%tuple(tmp[:5]))
                for k in xrange(Nums[0]*Nums[0]):
                    tmp1=[float(tp) for tp in f1.readline().split()]
                    tmp2=[float(tp) for tp in f2.readline().split()]
                    tmp=[tp1+tp2 for tp1,tp2,in zip(tmp1,tmp2)]
                    f3.write('%21.12E%21.12E\n'%(tmp[0],tmp[1]))
        f1.close()
        f2.close()
        f2.close()
class Optwplot(object):
    __slot__=['mode','orig','xend','yend','zend','grid',
              'ppo','pu','we','iskpt','iswann','wiw']
    def __init__(self,mode,orig,xend,yend,zend,grid,ppo,pu,we,iskpt,iswann,wiw):
        self.mode=mode; self.orig=tuple(orig+['orig']); self.grid=tuple(grid+[0]*3)
        self.xend=tuple(xend+['x-end']); self.yend=tuple(yend+['y-end'])
        self.zend=tuple(zend+['z-end']);self.pp=ppo 
        self.list1=(wiw,pu,we); self.wann=(iskpt,iswann,'')
    def wplotin(self,name): #----make wplotin flie--------------
        temp='%2d %2d %2d %d     #x, y, z, divisor of %s\n'
        f=open('%s.wplotin'%name,'w')
        f.write('3D ORTHO%9s mode O(RTHOGONAL)|N(ON-ORTHOGONAL)\n'%'#')
        f.write(temp%self.orig+temp%self.xend+temp%self.yend+temp%self.zend)
        f.write(' %d %d %d %d %d %d # grid points and echo increments\n'%self.grid)
        f.write('%-16s# DEP(HASING)|NO (POST-PROCESSING)\n'%self.ppo)
        f.write('%3s %3s %3s   # switch ANG|ATU|AU LARGE|SMALL\n'%self.list1)
        f.write('%d  %d%12s# k-point, band index' %self.wann)
        f.close()
#==============================start main program==============================
def main(ompt,m,s,op,osp,ow,owp):
    date()
    if ompt!=0: #-------------set OMP_NUM_THREADS------------------------------
        os.environ['OMP_NUM_THREADS']='%d'%ompt
        print('OMP_NUM_THREADS=%s'%os.environ['OMP_NUM_THREADS'])
    if op.run_sw==0 or op.sp==0:
        run_opt=''; lapw_opt=''
    else:
        lapw_opt='-c up'
        if (op.run_sw==1 or op.run_sw==4
            or op.run_sw=='spin' or op.run_sw=='LDA+U'):
            run_opt='sp_c' if nonmag else 'sp'
        elif op.run_sw==2 or op.run_sw=='AFM':
            run_opt='afm'
        elif op.run_sw==3 or op.run_sw=='FSM':
            run_opt='fsm'
    if s.wien==1 or s.rlapw==1: #------------scf start------------------
        prm_init,prm_run=op.options()
        if s.clean==1:   os.system('rm *.broyd*')
    if s.wien!=0:
        if s.sw_str==1:
            print('make struct file')
            if s.cif==1:
                m.cif2struct()
            else:
                m.struct()
    if s.wien==1:
        os_and_print('init_lapw  %s'%prm_init)
    elif s.wien==2:
        op.xkgen(); os.system('x dstart')
    elif s.wien==3:
        op.init(n_num)
    if op.so==1:
        shutil.copyfile('%s.in2'%m.name,'%s.in2c'%m.name)
        modin1(m.name)
        mkinso(m.name,lmax,so_emin,so_emax,nSO,n_RLO,e_lo,de,desw,m.atom)
        #os_and_print('symmetso')
    if s.rlapw==1:
        os_and_print('run'+run_opt+'_lapw %s'%prm_run)
    if s.dos==1: #-----------------calc dos---------------------------------
        print('start calc dos')
        os_and_print('x lapw2 -qtl'+(' -so' if so else ''))
        m.tetra_int(False)
        os_and_print('x tetra')
    if s.gspa==1 or s.wan==1: #----------calc spaghetti(WIEN2k band struct)---
        p_nam,p_num=klist_band(m.brav(0),s.t1)
        N,con=point_len(m.strc,p_num[:],osp.k_num,m.brav(0))
    if s.gspa==1:
        if os.path.isfile('%s.vector'%m.name)==True:
            shutil.move('%s.vector'%m.name,'%s.vector.kilst'%m.name)
            shutil.move('%s.energy'%m.name,'%s.energy.klist'%m.name)
        print(p_nam)
        print('make %s.klist_band'%m.name)
        mkklist(m.name,m.brav(0),p_nam,p_num,N,con)
        os_and_print('x lapw1 -band')
        if op.so==1:
            os_and_print('x lapwso')
        if s.oc==1 or s.ocall==1:
            shutil.move('%s.scf2'%m.name,'%s.scf2.old'%m.name)
            os_and_print('x lapw2 -band -qtl'+(' -so' if so else ''))
            shutil.move('%s.scf2'%m.name,'%s.scf2.qtl'%m.name)
            shutil.move('%s.scf2.old'%m.name,'%s.scf2'%m.name)
    if s.spa==1:
        if s.gspa!=1 and (os.path.isfile('%s.vector'%m.name)==True) and (
            os.path.isfile('%s.vector.band'%m.name)==True):
            shutil.move('%s.vector'%m.name,'%s.vector.kilst'%m.name)
            shutil.move('%s.energy'%m.name,'%s.energy.klist'%m.name)
            shutil.move('%s.vector.band'%m.name,'%s.vector'%m.name)
            shutil.move('%s.energy.band'%m.name,'%s.energy'%m.name)
        if s.oc==1 or s.ocall==1:
            shutil.move('%s.scf2'%m.name,'%s.scf2.old'%m.name)
            shutil.move('%s.scf2.qtl'%m.name,'%s.scf2'%m.name)
        if s.ocall==1:
            olist=m.get_olist();
            fnc=lambda x: atom[:x].count(atom[x])
            atm_list=[atm+('' if fnc(k)==0 else '%d'%(fnc(k)+1))
                      for k,atm in enumerate(atom)]
            for i,at in enumerate(atm_list):
                for j,ol in enumerate(olist[i]):
                    osp.param[0]=i+1; osp.param[1]=j+1
                    osp.insp(m.getef('scf2.old'),m.name)
                    os_and_print('x spaghetti'+(' -so' if op.so else ''))
                    shutil.move('%s.spaghetti_ps'%m.name,'%s_%s_%s.ps'
                                %(m.name,at,ol))
            osp.param=[0,0,0.2]
            osp.insp(m.getef('scf2.old'),m.name)
            os_and_print('x spaghetti'+(' -so' if op.so else ''))
            shutil.move('%s.spaghetti_ps'%m.name,'%s.ps'%m.name)
        else:
            osp.insp(m.getef('scf2.old' if s.oc else 'scf2'),m.name)
            os_and_print('x spaghetti'+(' -so' if op.so else ''))
        shutil.move('%s.vector'%m.name,'%s.vector.band'%m.name)
        shutil.move('%s.energy'%m.name,'%s.energy.band'%m.name)
        if s.oc==1 or s.ocall==1:
            shutil.move('%s.scf2'%m.name,'%s.scf2.qtl'%m.name)
            shutil.move('%s.scf2.old'%m.name,'%s.scf2'%m.name)
        if os.path.isfile('./%s.vector.klist'%m.name)==True:
            shutil.move('%s.vector.klist'%m.name,'%s.vector'%m.name)
            shutil.move('%s.energy.klist'%m.name,'%s.energy'%m.name)
    if s.wan_klist==1: #------------calc 1st BZ's klist for wannier---------
        shutil.copy('%s.struct'%m.name,'%s.ksym'%m.name)
        klist_wan(m.name,ow.num,m.brav(0))
        os_and_print('x lapw1')
        if op.so==1:
            for csp in ['up','dn']:
                shutil.copyfile('%s.vector'%m.name,'%s.vector%s'%(m.name,csp))
                shutil.copyfile('%s.vsp'%m.name,'%s.vsp%s'%(m.name,csp))
                shutil.copyfile('%s.vns'%m.name,'%s.vns%s'%(m.name,csp))
                shutil.copyfile('%s.energy'%m.name,'%s.energy%s'%(m.name,csp))
            os_and_print('x lapwso -up')
    if s.wan==1: #--------------------start wannier--------------------
        atnum=m.atom_num()
        if ow.wcan==[]:
            i=0
            for at in atnum:
                i=i+1
                for wat in ow.watom:
                    if wat==at[0] or wat==at[1]:
                        ow.wcan.append(i)
            print(ow.wcan)
        if s.w2w==1:
            print('make .win and w2win')
            if s.shift==0:
                ef=m.getef('scf2')*Ry
            else:
                ef=0.0
                if op.so==1:
                    so_opt='-soup'
                    shutil.copyfile('%s.scf2'%m.name,'%s.scf2up'%m.name)
                else:
                    so_opt=''                
                os_and_print('find_bands %s %s %f %f >%s.find_bands'
                  %(so_opt,m.name,ow.win_param[1]-1,ow.win_param[0]+1,m.name))
        num_b=m.get_band_num()
        ow.win(m.name,p_nam,p_num,num_b,0,op.so)
        if s.w2w==1:
            print('start wannier')
            ow.w2win(m.name,num_b,op.so)
            os_and_print('wannier90.x -pp %s'%m.name)
            if op.so==1:
                mk_wfdef(m.name)
                os_and_print('w2wc_global wfup.def')
                os_and_print('w2wc_global wfdn.def')
                ow.amn_total(m.name)
                ow.mmn_total(m.name)
            else:
                os_and_print('write_w2wdef %s'%m.name)
                os_and_print('w2w.o %s'%m.name)
            if s.shift==1:
                os_and_print('shift_energy %s'%m.name)
            os_and_print('wannier90.x  %s'%m.name)
    if s.gnu==1:
        outname='%s_%4.1fto%3.1f'%(m.name,ow.wmin,ow.wmax)
        get_eps(m.name,outname,ow.wmin-1,ow.wmax+1)
    if s.wplot==1: #-----------------out put xcrysden file for WF------------
        owp.wplotin(m.name)
        os_and_print('write_wplotdef %s'%m.name)
        os_and_print('prepare_plots.sh %s'%m.name)
        if s.clean==1:
            os_and_print('rm *.xsf.gz')
        os_and_print('xsfAll.sh %s'%m.name)
    date()
if __name__ == "__main__":
    name=os.getcwd().split('/')[-1]
    num_k=num_wien*num_wien*num_wien
    m=Material(name,space,atom,strc,astr,cifname)
    s=Switch()
    op=Opt()
    s.setup(wien,rlapw,dos,eden,xspec,gspa,spa,wan_klist,
             w2w,wan,wplot,gnu,oc,ocall,sw_str,shift,clean,t1,cif)
    op.setup(default,batch,num_k,vxc,rmt_red,ecut,rkmax,mix,fmt,sp,
           ec,cc,fc,loop_num,so,mpi,in1new,ni,run_sw)
    osp=Optspa(k_num,e_min,e_max,jatom,jtype,soh)
    ow=Optwan(win_min,win_max,f_min,f_max,num,watom,prj,fsp,bxsf_num)
    ow.wcan=wcan
    owp=Optwplot(mode,orig,xend,yend,zend,grid,ppo,pu,we,iskpt,iswann,wiw)
    main(ompt,m,s,op,osp,ow,owp)
#END PROGRAM w2w.py
  #========================================================================#
  # This program fork wien2wannier_jisaku developed by H. Usui and         #
  # testcase1.sh in wien2wannier developed by J. Kunes, P. Wissgott        #
  #                                                                        #
  # Copyright (c) 2010-2018  K. Suzuki                                     #
  #========================================================================#
  #                                LICENSE                                 #
  #========================================================================#
  # This program is free soft ware; you can redistribute it and/or modify  #
  # it under the terms of G General Public Licence as published by         #
  # the Free Software Foundation; either version 3 of the License, or      #
  # (at your option) any later version                                     #
  #                                                                        #
  # This program is distributed in the hope that it will be useful,        #
  # but WITHOUT ANY WARRANTY; without even the implied warranty of         #
  # MERCHANTABILITY or FIT A PARTICULAR PURPOSE. See the GNU               #
  # General Public License for more details.                               #
  #                                                                        #
  # You should hace received a copy of the GNU General Public License      #
  # along with this program; if not, write to the Free Software Foundation,#
  # Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                          #
  #========================================================================#
